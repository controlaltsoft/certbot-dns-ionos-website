# certbot-dns-ionos-website

IONOS website Authenticator plugin for Certbot.

Disclaimer: This software is not affiliated in any way to 1&1 IONOS, "IONOS" is a trademark of 1&1 IONOS.

This module uses IONOS website to complete DNS-01 challenge instead of the (Beta) DNS API as it is not available for EU customers.
Once this API is available, this module will be discontinued.

Please remember that IONOS website structure may change at any time, therefore this module may stop working at any time.


## Usage

First you need to install the module using `setup.py`:

```shell
$ sudo python3 ./setup.py install
```

Then you create a credentials file, for instance `ionos-credentials.ini`:
```ini
dns_ionos_website_endpoint = ionos.fr
dns_ionos_website_username = yourdomain.com
dns_ionos_website_password = yourpassword
```

It is preferable to give really strict rights to this file:
```shell
chmod 600 ionos-credentials.ini
```

Finally, you can use certbot to generate certificates using DNS-01 challenge method with IONOS website:

```shell
$ sudo certbot certonly \                                                                                                       ✔  6s  
    -d "*.yourdomain.com,yourdomain.com" \ 
    --email you@yourdomain.com \
    -a dns-ionos-website \
    --dns-ionos-website-credentials ionos-credentials.ini \
    --dns-ionos-website-propagation-seconds 30  
```

`--dns-ionos-website-propagation-seconds` is optional, its default value is 30 seconds.

#### Using an alternate user agent

The default user agent used to call the website is [python-requests' one](https://2.python-requests.org/en/master/).

You may override it using `CERTBOT_DNS_IONOS_WEBSITE_USER_AGENT` environment variable.

## Author

Benjamin Perche <b.perche@controlaltsoft.com>

## Licence

MIT (See [COPYING](COPYING))
