import logging
import requests

from .parsers import LoginPageParser, DnsRecordTableParser

try:
    # Python 3
    from urllib.parse import urlencode
except ImportError:
    # Python 2
    from urllib import urlencode


logger = logging.getLogger(__name__)


class IonosClient:
    def __init__(self, endpoint, username, password, user_agent, ttl):
        self.__session = requests.session()
        self.__endpoint = endpoint
        self.__username = username
        self.__password = password
        self.__ttl = ttl
        self.__authenticated = False

        if user_agent is not None:
            self.__session.headers['User-Agent'] = user_agent

    def __del__(self):
        if not self.__authenticated:
            return

        self.logout()

    def add_txt_record(self, domain, validation_name, validation):
        self._ensure_authentication()

        url = 'https://my.%s/add-dns-record/%s' % (self.__endpoint, domain)
        params = {
            '__sendingdata': '1',
            'record.forWwwSubdomain': 'true',
            'record.type': 'TXT',
            'record.host': validation_name,
            'record.value': validation,
            'record.ttl': 3600,
        }

        response = self.__session.post(url, data=params, allow_redirects=False)

        if response.status_code != 302:
            raise HttpError(
                "Failed to create challenge TXT entry, the following URL returned a "
                "%d status code (expecting 302): %s" % (
                    response.status_code,
                    url,
                )
            )
        elif response.headers['location'] != '/domain-dns-settings/%s' % domain:
            raise HttpError(
                "Failed to create challenge TXT entry, the endpoint returned a redirection to: %s; expected: %s" % (
                    response.headers['location'],
                    '/domain-dns-settings/%s' % domain,
                )
            )

    def del_txt_record(self, domain, validation_name, validation):
        logger.debug('Deleting DNS TXT entry for domains %s with validation_name=%s and validation=%s' % (
            domain,
            validation_name,
            validation,
        ))

        self._ensure_authentication()

        record_id = self._get_record_id(domain, validation_name, validation)

        url = 'https://my.%s/delete-dns-record/%s/%s' % (
            self.__endpoint,
            domain,
            record_id,
        )

        params = {
            '__sendingdata': '1',
            'delete.execute': 'true',
        }

        response = self.__session.post(url, data=params, allow_redirects=False)

        if response.status_code != 302:
            raise HttpError(
                "Failed to delete challenge TXT entry, the following URL returned a "
                "%d status code (expecting 302): %s" % (
                    response.status_code,
                    url,
                )
            )

    def _ensure_authentication(self):
        if not self.__authenticated:
            self._login()
            self.__authenticated = True

    def _login(self):
        url = 'https://login.%s/' % self.__endpoint

        params = self._get_login_params()
        response = self.__session.post(url, data=params, allow_redirects=False)
        content = response.content

        if isinstance(content, bytes):
            content = content.decode('utf-8')

        if response.status_code == 200:
            raise HttpError("Failed to login to IONOS, username or password may be invalid. HTTP body: " + content)
        elif response.status_code != 302:
            raise HttpError(
                "Failed to login to IONOS, the following URL returned a "
                "%d status code (expecting 302): %s. HTTP body: %s" % (
                    response.status_code,
                    url,
                    content,
                )
            )

        otk_response = self.__session.get(response.headers['location'], allow_redirects=False)

        if otk_response.status_code != 302:
            raise HttpError(
                "Failed to login to IONOS, the following URL returned a "
                "%d status code (expecting 302): %s" % (
                    otk_response.status_code,
                    response.headers['location'],
                )
            )

    def _get_login_params(self):
        url = 'https://login.%s/' % self.__endpoint
        response = self.__session.get(url, allow_redirects=False)

        if response.status_code != 200:
            raise HttpError(
                "Failed to login to IONOS, the following URL returned a "
                "%d status code (expecting 200): %s" % (
                    response.status_code,
                    url,
                )
            )

        content = response.content

        if isinstance(content, bytes):
            content = content.decode('utf-8')

        parser = LoginPageParser()
        parser.feed(content)

        params = parser.params.copy()

        if 'oaologin.csrf' not in params:
            raise HttpError("Failed to read IONOS login form")

        params['oaologin.username'] = self.__username
        params['oaologin.password'] = self.__password

        return params

    def logout(self):
        url = 'https://my.%s/logout' % self.__endpoint

        try:
            self.__session.get(url, allow_redirects=False)
        except:  # ignore all exceptions, we're only logging out.
            pass

        self.__authenticated = False

    def _get_record_id(self, domain, validation_name, validation):
        url_params = {
            '__render_href': 'txt/pages/domain-dns-settings.xml',
            '__render_part': 'table-component-body',
            '__render_module': 'controlpanel-frontend-core',
            'page.sort': 'service,desc',
            'page.size': '10',
            'filter.search': validation_name,
        }

        url = 'https://my.%s/domain-dns-settings/%s?%s' % (
            self.__endpoint,
            domain,
            urlencode(url_params),
        )

        response = self.__session.get(url, allow_redirects=False)
        content = response.content

        if isinstance(content, bytes):
            content = content.decode('utf-8')

        logger.debug('Partial table from %s: %s' % (url, content))
        parser = DnsRecordTableParser()
        parser.feed(content)

        records = [record for record in parser.records if validation in record['value']]

        if len(records) == 0:
            raise HttpError("Failed to get the existing record of the ACME challenge")
        elif len(records) > 1:
            raise HttpError("More than one record matches the ACME challenge")

        return records[0]['id']


class HttpError(BaseException):
    pass
