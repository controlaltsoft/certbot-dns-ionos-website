"""DNS Authenticator for IONOS DNS."""
import logging
import os

from typing import Optional

from certbot import errors
from certbot.plugins import dns_common
from certbot.plugins.dns_common import CredentialsConfiguration

from .client import IonosClient, HttpError

logger = logging.getLogger(__name__)


def remove_suffix(validation_name, domain):
    suffix = '.' + domain

    if validation_name.endswith(suffix):
        return validation_name[0:-len(suffix)]

    return validation_name


class Authenticator(dns_common.DNSAuthenticator):
    """DNS Authenticator for IONOS.
    This Authenticator uses the IONOS website to fulfill a dns-01 challenge.
    """

    description = 'Obtain certificates using a DNS TXT record (if you are using IONOS for DNS).'
    ttl = 60

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.credentials: Optional[CredentialsConfiguration] = None
        self.__client = None

    def __del__(self):
        if self.__client is not None:
            self.__client.logout()

    @classmethod
    def add_parser_arguments(cls, add, default_propagation_seconds=10):  # pylint: disable=arguments-differ
        super().add_parser_arguments(add, default_propagation_seconds=30)
        add('credentials', help='IONOS credentials INI file.')

    def more_info(self):  # pylint: disable=missing-function-docstring
        return 'This plugin configures a DNS TXT record to respond to a dns-01 challenge using ' + \
               'the IONOS website.'

    def _setup_credentials(self):
        self.credentials = self._configure_credentials(
            'credentials',
            'IONOS credentials INI file',
            {
                'endpoint': 'IONOS endpoint (ionos.fr)',
                'username': 'Your IONOS username',
                'password': 'Your IONOS password',
            }
        )

    def _perform(self, domain, validation_name, validation):
        try:
            self._get_ionos_client().add_txt_record(domain, remove_suffix(validation_name, domain), validation)
        except HttpError as error:
            raise errors.PluginError(str(error))

    def _cleanup(self, domain, validation_name, validation):
        try:
            self._get_ionos_client().del_txt_record(domain, remove_suffix(validation_name, domain), validation)
        except HttpError as error:
            logger.error(str(error))

    def _get_ionos_client(self):
        if self.__client is None:
            if not self.credentials:  # pragma: no cover
                raise errors.Error("Plugin has not been prepared.")

            user_agent = os.getenv('CERTBOT_DNS_IONOS_WEBSITE_USER_AGENT')

            if not user_agent:
                user_agent = None

            self.__client = IonosClient(
                self.credentials.conf('endpoint'),
                self.credentials.conf('username'),
                self.credentials.conf('password'),
                user_agent,
                self.ttl,
            )

        return self.__client
