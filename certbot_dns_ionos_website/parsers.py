from html.parser import HTMLParser


def append_strip(dictionary, key, value):
    if dictionary[key] is None:
        dictionary[key] = value
    else:
        dictionary[key] += value

    dictionary[key] = dictionary[key].strip()


class LoginPageParser(HTMLParser):
    LOGIN_FORM_FIELDS = (
        '__lf',
        '__sendingdata',
        'oaologin.fp',  # seems like some data is loaded here via javascript, but is not needed for login
        'oaologin.csrf',
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.in_login_form = False
        self.params = {}
        self.error_message = None

    def error(self, message):
        self.error_message = message

    def handle_starttag(self, tag, attrs):
        indexed_attrs = dict(attrs)
        if tag == 'form' and 'id' in indexed_attrs and indexed_attrs['id'] == 'login-form':
            self.in_login_form = True
        elif self.in_login_form and tag == 'input':
            if 'name' in indexed_attrs and indexed_attrs['name'] in self.LOGIN_FORM_FIELDS:
                self.params[indexed_attrs['name']] = indexed_attrs['value'] if 'value' in indexed_attrs else ''

    def handle_endtag(self, tag):
        if self.in_login_form and tag == 'form':
            self.in_login_form = False


class DnsRecordTableParser(HTMLParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__current_row = None
        self.__current_cell = 0
        self.__in_cell = False
        self.records = []
        self.error_message = None

    def error(self, message):
        self.error_message = message

    def handle_data(self, data):
        if self.__in_cell:
            if self.__current_cell == 1:
                append_strip(self.__current_row, 'type', data)
            elif self.__current_cell == 2:
                append_strip(self.__current_row, 'host', data)
            elif self.__current_cell == 3:
                append_strip(self.__current_row, 'value', data)

    def handle_starttag(self, tag, attrs):
        indexes_attrs = dict(attrs)

        if tag == 'tr' and ('id' not in attrs or indexes_attrs['id'] != 'bulk-actions'):
            self.__current_row = {
                'type': None,
                'host': None,
                'value': None,
                'id': None,
            }
        elif tag == 'td' and self.__current_row is not None:
            self.__in_cell = True
        elif self.__in_cell:
            if tag == 'input' and self.__current_cell == 0:
                self.__current_row['id'] = indexes_attrs['value'] if 'value' in indexes_attrs else None

    def handle_endtag(self, tag):
        if tag == 'tr' and self.__current_row is not None:
            if self.__current_row['id'] is not None and self.__current_row['value'] is not None:
                self.records.append(self.__current_row)

            self.__current_row = None
            self.__current_cell = 0
        elif tag == 'td':
            self.__current_cell += 1
            self.__in_cell = False
